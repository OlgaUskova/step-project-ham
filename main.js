$(function () {
    // Owl Carousel
    const owl = $(".owl-carousel");
    owl.owlCarousel({
        items: 4,
        margin: 10,
        loop: true,
        nav: true
    });

    $('.owl-item img').on('click', function (event) {
        let imgSrc = $(event.target).attr('src'),
            name = $(event.target).attr('data-name'),
            profession = $(event.target).attr('data-profession');

        $('.slider-current img').attr('src', imgSrc);
        $('.name span').text(name);
        $('.profession p').text(profession);
        $('.owl-item img').css('top', '0');
        $(event.target).css('position', 'absolute').css('top', '-15px').css('right', '14px')
    })
});


let tabs = document.querySelector(".section1_main-content-wrap_menu-wrap");
let tabsContent = document.querySelector(".section1_main-content-wrap_info-box");


function start() {
    for (let i = 0; i < tabs.children.length; i++) {
        tabs.children[i].dataset.id = `${i + 1}`;
    }
    for (let i = 0; i < tabsContent.children.length; i++) {
        tabsContent.children[i].dataset.id = `${i + 1}`;

        if (i === 0) {
            tabsContent.children[i].classList.add('active')
        } else {
            tabsContent.children[i].style.display = 'none'
        }
    }
}

start();

tabs.addEventListener("click", showTab);

function showTab(event) {
    let className = "active";
    let tab = event.target.closest("li");

    for (const li of tabs.children) {
        li.classList.remove(className);
    }
    tab.classList.add(className);

    for (const li of tabsContent.children) {
        if (li.dataset.id === tab.dataset.id) {
            li.style.display = 'block';
        } else {
            li.style.display = 'none';
        }
    }
}

let tabsFilter = document.querySelector(".section2_main-content-wrap_menu-wrap");
let cardsFilter = document.querySelector(".section2_main-content-wrap_gallery-wrap");
let button = document.querySelector('.section2_main-content-wrap_button');
let section2 = document.querySelector('.section2');

tabsFilter.addEventListener("click", filterCards);

button.addEventListener('click', showMoreImages);

document.querySelector('[data-filter="all"]').click();

function showMoreImages() {
    let currentTab = tabsFilter.querySelector('.selected');
    let cardsCount = 0;


    if (currentTab === null || currentTab.dataset.filter === 'all') {
        cardsCount = cardsFilter.children.length;
        for (let i = 0; i < cardsFilter.children.length; i++) {
            cardsFilter.children[i].style.display = 'block';
        }

    } else {

        for (let i = 0; i < cardsFilter.children.length; i++) {
            if (cardsFilter.children[i].dataset.filter === currentTab.dataset.filter) {
                cardsFilter.children[i].style.display = 'block';
                cardsCount++;
            }
        }

        section2.style.paddingBottom = '30px';
    }
    hideOrShowLoadMoreButton(cardsCount, true);
}

function filterCards(event) {

    let className = "selected";
    let currentTab = event.target.closest("li");
    let cardsCount = 0;

    for (const li of tabsFilter.children) {
        li.classList.remove(className);
    }
    currentTab.classList.add(className);


    if (currentTab.dataset.filter === 'all') {
        cardsCount = cardsFilter.children.length;

        for (let i = 0; i < cardsFilter.children.length; i++) {
            cardsFilter.children[i].style.display = 'none';

            if (i < 12) {
                cardsFilter.children[i].style.display = 'block';
            }
        }
    } else {
        for (let i = 0; i < cardsFilter.children.length; i++) {
            cardsFilter.children[i].style.display = 'none';

            if (cardsFilter.children[i].dataset.filter === currentTab.dataset.filter) {

                if (cardsCount < 12) {
                    cardsFilter.children[i].style.display = 'block';
                }
                cardsCount++;
            }
        }
    }

    hideOrShowLoadMoreButton(cardsCount);

    section2.style.paddingBottom = '30px';
}

function hideOrShowLoadMoreButton(cardsCount, isShowMore = false) {
    if (isShowMore) {
        button.style.display = 'none'
    } else {
        if (cardsCount > 12) {
            button.style.display = 'block';
        } else {
            button.style.display = 'none'
        }
    }

}
